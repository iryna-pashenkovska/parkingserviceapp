﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ParkingAppClient
{
    class Program
    {
        public static HttpClient client = new HttpClient();

        static void Main(string[] args)
        {
            // Update port # in the following line.
            client.BaseAddress = new Uri("http://localhost:51154/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            MenuHandler.DrawMenu();
            bool cont = true;
            do
            {
                try
                {
                    Console.WriteLine();
                    Console.Write("Your choise: ");
                    var action = Console.ReadLine();
                    Console.Clear();
                    MenuHandler.DrawMenu();
                    cont = MenuHandler.SelectMenuOption(action);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            } while (cont);
        }
    }
}
