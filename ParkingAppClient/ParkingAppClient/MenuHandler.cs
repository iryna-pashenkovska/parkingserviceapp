﻿using Newtonsoft.Json;
using ParkingAppClient.Entities;
using ParkingAppCommon;
using ParkingAppCommon.Exceptions;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ParkingAppClient
{
    public class MenuHandler
    {
        private static void DrawStarLine()
        {
            Console.WriteLine("***********************");
        }
        private static void DrawTitle()
        {
            DrawStarLine();
            Console.WriteLine("+++   PARKING APP   +++");
            DrawStarLine();
        }
        public static void DrawMenu()
        {
            DrawTitle();
            DrawStarLine();
            Console.WriteLine(" 1. Parking balance");
            Console.WriteLine(" 2. Money earned in last minute");
            Console.WriteLine(" 3. Show number of free/occuppied places");
            Console.WriteLine(" 4. Show all transactions made in last minute");
            Console.WriteLine(" 5. Show all transactions");
            Console.WriteLine(" 6. Show list of all vehicles");
            Console.WriteLine(" 7. Put vehicle on parking place");
            Console.WriteLine(" 8. Pick up vehicle from parking place");
            Console.WriteLine(" 9. Recharge the balance of the vehicle");
            DrawStarLine();
            Console.WriteLine("Make your choice: type 1, 2,... or {0} for exit", 0);
            DrawStarLine();
        }

        public static bool SelectMenuOption(string action)
        {
            bool cont = true;
            switch (action.Trim())
            {
                case "1":
                    ShowParkingBalance().GetAwaiter().GetResult();
                    break;
                case "2":
                    EarnedLastMinute().GetAwaiter().GetResult();
                    break;
                case "3":
                    ShowParkingPlacesStatus().GetAwaiter().GetResult();
                    break;
                case "4":
                    ShowLastMinuteTransactions().GetAwaiter().GetResult();
                    break;
                case "5":
                    ShowAllTransactions().GetAwaiter().GetResult();
                    break;
                case "6":
                    ShowVehicles().GetAwaiter().GetResult();
                    break;
                case "7":
                    PutVehicleOnParking().GetAwaiter().GetResult();
                    break;
                case "8":
                    PickUpVehicleFromParking().GetAwaiter().GetResult();
                    break;
                case "9":
                    RechargeVehicleBalance().GetAwaiter().GetResult();
                    break;
                case "0":
                    cont = false;
                    DeleteTransactionLogAsync().GetAwaiter().GetResult();
                    break;
            }

            return cont;
        }

        //
        // #1. Parking balance
        //
        private static async Task ShowParkingBalance()
        {
            double balance = await GetParkingBalanceAsync();
            Console.WriteLine("Parking balance = {0}", balance);
        }

        private static async Task<double> GetParkingBalanceAsync()
        {
            double parkingBalance = 0;
            HttpResponseMessage response = await Program.client.GetAsync("api/parking/balance");
            if (response.IsSuccessStatusCode)
            {
                parkingBalance = await response.Content.ReadAsAsync<double>();
            }
            return parkingBalance;
        }

        //
        // #2.  Money earned in last minute
        //
        private static async Task EarnedLastMinute()
        {
            double lastMinuteProfit = await GetEarnedLastMinute();
            Console.WriteLine("Earned during last minute = {0}", lastMinuteProfit);
        }

        private static async Task<double> GetEarnedLastMinute()
        {
            double earnedLastMinute = 0;
            HttpResponseMessage response = await Program.client.GetAsync("api/parking/lastminuteprofit");
            if (response.IsSuccessStatusCode)
            {
                earnedLastMinute = await response.Content.ReadAsAsync<double>();
            }
            return earnedLastMinute;
        }

        //
        // #3. Show number of free/occuppied places
        //
        private static async Task ShowParkingPlacesStatus()
        {
            int freePlaces = await GetParkingFreePlacesNumber();
            int occupiedPlaces = await GetParkingOccupiedPlacesNumber();
            Console.WriteLine("Free - {0}, Occupied - {1}", freePlaces, occupiedPlaces);
        }

        private static async Task<int> GetParkingFreePlacesNumber()
        {
            int freePlaces = 0;
            HttpResponseMessage response = await Program.client.GetAsync("api/parking/freeplaces");
            if (response.IsSuccessStatusCode)
            {
                freePlaces = await response.Content.ReadAsAsync<int>();
            }
            return freePlaces;
        }

        private static async Task<int> GetParkingOccupiedPlacesNumber()
        {
            int occuppiedPlaces = 0;
            HttpResponseMessage response = await Program.client.GetAsync("api/parking/occupiedplaces");
            if (response.IsSuccessStatusCode)
            {
                occuppiedPlaces = await response.Content.ReadAsAsync<int>();
            }
            return occuppiedPlaces;
        }

        //
        // #4. Show all transactions made in last minute
        //
        private static async Task ShowLastMinuteTransactions()
        {
            List<Transaction> transactions = await GetLastMinuteTransactions();
            ShowTransactions(transactions);
        }

        private static async Task<List<Transaction>> GetLastMinuteTransactions()
        {
            var transactions = await Program.client.GetStringAsync("api/transactions/donelastminute");
            return JsonConvert.DeserializeObject<List<Transaction>>(transactions);
        }

        //
        // #5. Show all transactions
        //
        private static async Task ShowAllTransactions()
        {
            List<Transaction> transactions = await GetAllTransactions();
            ShowTransactions(transactions);
        }

        private static async Task<List<Transaction>> GetAllTransactions()
        {
            var transactions = await Program.client.GetStringAsync("api/transactions");
            return JsonConvert.DeserializeObject<List<Transaction>>(transactions);
        }

        private static void ShowTransactions(List<Transaction> transactions)
        {
            if (transactions.Count == 0)
            {
                Console.WriteLine("No transactions saved yet.");
            }
            else
            {
                foreach (var transaction in transactions)
                {
                    Console.WriteLine(transaction.ToString());
                }
            }
        }

        //
        // #6. Show list of all vehicles
        //
        private static async Task ShowVehicles()
        {
            List<Vehicle> vehicles = await GetVehiclesListAsync();

            if (vehicles.Count == 0)
            {
                Console.WriteLine("Parking is empty.");
            }
            else
            {
                foreach (var vehicle in vehicles)
                {
                    Console.WriteLine(vehicle.ToString());
                }
            }
        }

        private static async Task<List<Vehicle>> GetVehiclesListAsync()
        {
            var vehicles = await Program.client.GetStringAsync("api/vehicles");
            return JsonConvert.DeserializeObject<List<Vehicle>>(vehicles);
        }

        //
        // #7. Put vehicle on parking place
        //
        private static async Task PutVehicleOnParking()
        {
            if (0 == await GetParkingFreePlacesNumber())
                throw new MaxCapacityReachedException();

            string vehicleType = "";
            string vehicleBalance = "";

            try
            {
                vehicleType = AskUser("vehicle type").ToLower();
                var vt = (VehicleType)Enum.Parse(typeof(VehicleType), vehicleType);
                vehicleBalance = AskUser("vehicle balance");
                var vb = Convert.ToDouble(vehicleBalance);

                if(vb < 0)
                    throw new NegativeAmountException(vb);

                var newVehicle = new Vehicle(vt, vb);

                await CreateVehicleAsync(newVehicle);
            }
            catch (FormatException)
            {
                throw new NotANumberException(vehicleBalance);
            }
            catch (ArgumentException)
            {
                throw new InvalidVehicleTypeException(vehicleType);
            }
        }

        private static async Task CreateVehicleAsync(Vehicle vehicle)
        {
            HttpResponseMessage response = await Program.client.PostAsJsonAsync(
                "api/vehicles", vehicle);
            response.EnsureSuccessStatusCode();
        }

        //
        // #8. Pick up vehicle from parking place
        //
        private static async Task PickUpVehicleFromParking()
        {
            var vehicleId = AskUser("vehicle ID").ToLower();
            await DeleteVehicleAsync(vehicleId);
        }

        private static async Task DeleteVehicleAsync(string id)
        {
            HttpResponseMessage response = await Program.client.DeleteAsync($"api/vehicles/{id}");
            response.EnsureSuccessStatusCode();
        }

        //
        // #9. Recharge the balance of the vehicle
        //
        private static async Task RechargeVehicleBalance()
        {
            string vehicleRechargeAmount = "";

            try
            {
                var vehicleId = AskUser("vehicle ID").ToLower();
                vehicleRechargeAmount = AskUser(string.Format("amount for recharging vehicle {0}", vehicleId));
                var ra = Convert.ToDouble(vehicleRechargeAmount);

                if(ra < 0)
                    throw new NegativeAmountException(ra);

                Vehicle vehicle = await GetVehicleByIdAsync(vehicleId);

                if(vehicle == null)
                    throw new VehicleNotFoundException(vehicleId);

                vehicle.Balance += ra;
                await UpdateVehicleAsync(vehicle);
            }
            catch (FormatException)
            {
                throw new NotANumberException(vehicleRechargeAmount);
            }

        }

        private static async Task<Vehicle> GetVehicleByIdAsync(string id)
        {
            var vehicle = await Program.client.GetStringAsync($"api/vehicles/{id}");
            return JsonConvert.DeserializeObject<Vehicle>(vehicle);
        }

        private static async Task UpdateVehicleAsync(Vehicle vehicle)
        {
            HttpResponseMessage response = await Program.client.PutAsJsonAsync($"api/vehicles/{vehicle.VehicleId}", vehicle);
            response.EnsureSuccessStatusCode();
        }

        private static string AskUser(string info)
        {
            Console.WriteLine("Please enter {0}: ", info);
            return Console.ReadLine();
        }

        private static async Task DeleteTransactionLogAsync()
        {
            HttpResponseMessage response = await Program.client.DeleteAsync($"api/transactions");
            response.EnsureSuccessStatusCode();
        }
    }
}
