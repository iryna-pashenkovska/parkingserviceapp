﻿using Newtonsoft.Json;
using System;

namespace ParkingAppClient.Entities
{
    public class Transaction
    {
        [JsonProperty("transactionTime")]
        public DateTime TransactionTime { get; set; }
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }
        [JsonProperty("paymentAmount")]
        public double PaymentAmount { get; set; }

        public override string ToString()
        {
            return TransactionTime.ToString() + "," + VehicleId + "," + PaymentAmount.ToString();
        }
    }
}
