﻿using Newtonsoft.Json;
using ParkingAppCommon;
using System.Timers;

namespace ParkingAppClient.Entities
{
    public class Vehicle
    {
        private static int idGenerator = 0;

        [JsonProperty("timer")]
        public Timer timer { get; set; }
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }
        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; set; }
        [JsonProperty("balance")]
        public double Balance { get; set; }

        public Vehicle(){}

        public Vehicle(VehicleType vt, double bal)
        {
            VehicleType = vt;
            Balance = bal;

            idGenerator++;
            VehicleId = VehicleType.ToString() + "_" + idGenerator.ToString();
        }

        public override string ToString()
        {
            return VehicleId + " " + VehicleType.ToString() + " " + Balance;
        }
    }
}
