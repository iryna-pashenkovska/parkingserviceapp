﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingAppCommon.Exceptions
{
    public class MaxCapacityReachedException : Exception
    {
        public MaxCapacityReachedException() :
            base(String.Format("No free places on the Parking."))
        {

        }
    }
}
