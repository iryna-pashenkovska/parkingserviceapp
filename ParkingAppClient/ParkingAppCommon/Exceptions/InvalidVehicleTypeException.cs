﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingAppCommon.Exceptions
{
    public class InvalidVehicleTypeException : ArgumentException
    {
        public InvalidVehicleTypeException()
        {

        }

        public InvalidVehicleTypeException(string vehicleType)
            : base(String.Format("Invalid Vehicle Type: {0}", vehicleType))
        {

        }
    }
}
