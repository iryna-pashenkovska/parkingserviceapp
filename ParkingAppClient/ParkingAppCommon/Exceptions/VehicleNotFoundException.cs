﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ParkingAppCommon.Exceptions
{
    public class VehicleNotFoundException : Exception
    {
        public VehicleNotFoundException()
        {

        }

        public VehicleNotFoundException(string vehicleId)
            : base(String.Format("Vehicle with ID '{0}' doesn't exist.", vehicleId))
        {

        }
    }
}
