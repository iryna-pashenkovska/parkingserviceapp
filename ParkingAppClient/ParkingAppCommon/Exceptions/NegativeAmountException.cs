﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingAppCommon.Exceptions
{
    public class NegativeAmountException: Exception
    {
        public NegativeAmountException()
        {

        }

        public NegativeAmountException(double amount)
            : base(String.Format("Amount couldn't be negative: {0}", amount))
        {

        }
    }
}
