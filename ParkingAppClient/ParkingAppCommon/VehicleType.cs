﻿namespace ParkingAppCommon
{
    public enum VehicleType
    {
        bus = 1,
        car = 2,
        motorcycle = 3,
        truck = 4
    };
}
