﻿using ParkingAppServer.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace ParkingAppServer.Entities
{
    public sealed class Parking
    {
        private static readonly Lazy<Parking> lazy = new Lazy<Parking>(() => new Parking());
        public static Parking ParkingInstance { get { return lazy.Value; } }

        public double balance;
        public List<Vehicle> parkedVehicles;
        private List<Transaction> transactionLogLastMinute;
        public Timer timer;
        public TransacionLog transactionLog;

        public List<Transaction> TransactionLogLastMinute
        {
            get
            {
                return transactionLogLastMinute = transactionLogLastMinute.Where(t => t.transactionTime > DateTime.Now.AddMinutes(-1)).ToList();
            }
        }

        private Parking()
        {
            balance = ParkingAppSettings.initialParkingBalance;
            transactionLogLastMinute = new List<Transaction>();
            parkedVehicles = new List<Vehicle>();
            transactionLog = new TransacionLog(this);
        }
    }
}
