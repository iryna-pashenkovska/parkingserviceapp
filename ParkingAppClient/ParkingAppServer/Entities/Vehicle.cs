﻿using Newtonsoft.Json;
using ParkingAppCommon;
using System.Timers;

namespace ParkingAppServer.Entities
{
    public class Vehicle
    {
        [JsonProperty("vehicleType")]
        public VehicleType vehicleType { get; set; }
        [JsonProperty("balance")]
        public double balance { get; set; }
        [JsonProperty("timer")]
        public Timer timer { get; set; }
        [JsonProperty("vehicleId")]
        public string vehicleId { get; set; }

        public override string ToString()
        {
            return vehicleId + " " + vehicleType.ToString() + " " + balance.ToString();
        }
    }
}
