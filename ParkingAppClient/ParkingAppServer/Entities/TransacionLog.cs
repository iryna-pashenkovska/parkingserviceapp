﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;

namespace ParkingAppServer.Entities
{
    public class TransacionLog
    {
        public static string logFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Transaction.log");

        public TransacionLog(Parking parking)
        {
            parking.timer = new Timer(60000);
            parking.timer.Elapsed += WriteTransactionsToLogFile;
            parking.timer.AutoReset = true;
            parking.timer.Enabled = true;
            parking.timer.Start();
        }

        private void WriteTransactionsToLogFile(object sender, ElapsedEventArgs e)
        {
            if (Parking.ParkingInstance.TransactionLogLastMinute != null)
            {
                try
                {
                    using (StreamWriter outputFile = new StreamWriter(logFilePath, true))
                    {
                        foreach (var transaction in Parking.ParkingInstance.TransactionLogLastMinute)
                            outputFile.WriteLine(JsonConvert.SerializeObject(transaction));
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                }
            }
        }

        public static List<Transaction> ReadTransactionsFromLogFile()
        {
            List <Transaction> transactions = new List<Transaction>();
            try
            {
                using (StreamReader sr = new StreamReader(logFilePath))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        transactions.Add(JsonConvert.DeserializeObject<Transaction>(line));
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("Problem with reading from file");
                Console.WriteLine(e.Message);
            }

            return transactions;
        }
    }
}
