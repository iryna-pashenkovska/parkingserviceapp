﻿using ParkingAppServer.Entities;
using System.Collections.Generic;

namespace ParkingAppServer.Interfaces
{
    public interface ITransactionService
    {
        List<Transaction> GetLastMinuteTransactions();
        List<Transaction> GetAllTransactions();
        void StartTransactionsGeneration(Vehicle v);
        void AddTransaction(Transaction t);
        void DeleteTransactionLog();
    }
}
