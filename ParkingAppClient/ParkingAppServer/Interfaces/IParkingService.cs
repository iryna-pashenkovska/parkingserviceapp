﻿namespace ParkingAppServer.Interfaces
{
    public interface IParkingService
    {
        double GetBalance();
        double GetLastMinuteProfit();
        int GetFreePlacesNumber();
        int GetOccupiedPlacesNumber();
    }
}
