﻿using ParkingAppServer.Entities;
using System.Collections.Generic;

namespace ParkingAppServer.Interfaces
{
    public interface IVehicleService
    {
        List<Vehicle> GetVehicleList();
        Vehicle GetVehicleById(string vehicleId);
        void AddVehicle(Vehicle v);
        void UpdateVehicle(Vehicle newVehicleObject);
        void RemoveVehicle(string id);
    }
}
