﻿using ParkingAppServer.Entities;
using ParkingAppServer.Helpers;
using ParkingAppServer.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;

namespace ParkingAppServer.Services
{
    public class TransactionService: ITransactionService
    {
        private Parking parking = Parking.ParkingInstance;

        public void StartTransactionsGeneration(Vehicle v)
        {
            v.timer = new Timer(ParkingAppSettings.paymentTimeIntervalInSeconds * 1000);
            v.timer.Elapsed += (sender, e) => CreateTransaction(sender, e, v); ;
            v.timer.AutoReset = true;
            v.timer.Enabled = true;
            v.timer.Start();
        }

        public List<Transaction> GetLastMinuteTransactions()
        {
            return parking.TransactionLogLastMinute;
        }

        public List<Transaction> GetAllTransactions()
        {
            return TransacionLog.ReadTransactionsFromLogFile();
        }

        public void AddTransaction(Transaction t)
        {
            parking.TransactionLogLastMinute.Add(t);
        }

        private void CreateTransaction(object sender, ElapsedEventArgs e, Vehicle v)
        {
            double amount = 0;
            if (v.balance > ParkingAppSettings.rates[v.vehicleType])
                amount = ParkingAppSettings.rates[v.vehicleType];
            else
                amount = ParkingAppSettings.rates[v.vehicleType] * ParkingAppSettings.penaltyRatio;

            var transaction = new Transaction(DateTime.Now, v.vehicleId, amount);
            AddTransaction(transaction);
            parking.balance += amount;
            v.balance -= amount;
        }

        public void DeleteTransactionLog()
        {
            if (File.Exists(TransacionLog.logFilePath))
            {
                File.Delete(TransacionLog.logFilePath);
            }
        }
    }
}
