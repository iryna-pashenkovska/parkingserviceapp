﻿using System;
using ParkingAppCommon.Exceptions;
using ParkingAppServer.Entities;
using ParkingAppServer.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace ParkingAppServer.Services
{
    public class VehicleService: IVehicleService
    {
        private static Parking parking = Parking.ParkingInstance;
        private TransactionService transactionService = new TransactionService();

        public List<Vehicle> GetVehicleList()
        {
            return parking.parkedVehicles;
        }

        public void AddVehicle(Vehicle v)
        {
            transactionService.StartTransactionsGeneration(v);
            parking.parkedVehicles.Add(v);
        }

        public Vehicle GetVehicleById(string vehicleId)
        {
            var vehicle = parking.parkedVehicles.Find(v => v.vehicleId == vehicleId);

            if (vehicle == null)
                throw new VehicleNotFoundException(vehicleId);

            return vehicle;
        }

        public void UpdateVehicle(Vehicle newVehicleObject)
        {
            Vehicle oldVehicleObject = GetVehicleById(newVehicleObject.vehicleId);
            int index = parking.parkedVehicles.IndexOf(oldVehicleObject);
            if (index != -1)
                CopyPropertiesTo(newVehicleObject, parking.parkedVehicles[index]);
            else
                throw new VehicleNotFoundException(newVehicleObject.vehicleId);
        }

        private void CopyPropertiesTo<T, TU>(T source, TU dest)
        {
            var sourceProps = typeof(T).GetProperties().Where(x => x.CanRead).ToList();
            var destProps = typeof(TU).GetProperties().Where(x => x.CanWrite).ToList();

            foreach (var sourceProp in sourceProps)
            {
                if (destProps.Any(x => x.Name == sourceProp.Name))
                {
                    var p = destProps.First(x => x.Name == sourceProp.Name);
                    if (p.CanWrite)
                        p.SetValue(dest, sourceProp.GetValue(source, null), null);
                }

            }

        }

        public void RemoveVehicle(string id)
        {
            Vehicle v = GetVehicleById(id);
            v.timer.Stop();
            parking.parkedVehicles.Remove(v);
        }
    }
}
