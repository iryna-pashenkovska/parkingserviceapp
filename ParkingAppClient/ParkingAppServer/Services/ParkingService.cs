﻿using ParkingAppServer.Entities;
using ParkingAppServer.Helpers;
using ParkingAppServer.Interfaces;
using System.Linq;

namespace ParkingAppServer.Services
{
    public class ParkingService: IParkingService
    {
        private Parking parking = Parking.ParkingInstance;

        public double GetBalance()
        {
            return parking.balance;
        }

        public double GetLastMinuteProfit()
        {
            return parking.TransactionLogLastMinute.Select(t => t.paymentAmount).Sum();
        }

        public int GetFreePlacesNumber()
        {
            return ParkingAppSettings.maxParkingCapacity - GetOccupiedPlacesNumber();
        }

        public int GetOccupiedPlacesNumber()
        {
            return parking.parkedVehicles.Count;
        }
    }
}
