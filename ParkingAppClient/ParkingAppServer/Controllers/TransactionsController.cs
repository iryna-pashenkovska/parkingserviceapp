﻿using Microsoft.AspNetCore.Mvc;
using ParkingAppServer.Entities;
using ParkingAppServer.Interfaces;
using System.Collections.Generic;

namespace ParkingAppServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private ITransactionService _transactionService;

        public TransactionsController(ITransactionService service)
        {
            _transactionService = service;
        }

        // GET: api/transactions
        [HttpGet]
        public ActionResult<List<Transaction>> GetAllTransactions()
        {
            return _transactionService.GetAllTransactions();
        }

        // GET: api/transactions/donelastminute
        [Route("donelastminute")]
        [HttpGet]
        public ActionResult<List<Transaction>> GetLastMinuteTransactions()
        {
            return _transactionService.GetLastMinuteTransactions();
        }

        // DELETE api/transactions/
        [HttpDelete]
        public void DeleteAllTransactions()
        {
            _transactionService.DeleteTransactionLog();
        }
    }
}
