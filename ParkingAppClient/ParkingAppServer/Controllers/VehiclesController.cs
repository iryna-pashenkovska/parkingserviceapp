﻿using Microsoft.AspNetCore.Mvc;
using ParkingAppServer.Entities;
using ParkingAppServer.Interfaces;
using System.Collections.Generic;
using ParkingAppCommon.Exceptions;

namespace ParkingAppServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private IVehicleService _vehiclesService;

        public VehiclesController(IVehicleService service)
        {
            _vehiclesService = service;
        }

        // GET: api/vehicles
        [HttpGet]
        public ActionResult<List<Vehicle>> GetVehicleList()
        {
            return _vehiclesService.GetVehicleList();
        }

        // GET: api/vehicles/bus_1
        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicleById(string id)
        {
            try
            {
                return Ok(_vehiclesService.GetVehicleById(id));
            }
            catch(VehicleNotFoundException)
            {
                return new StatusCodeResult(500);
            }
        }

        // POST: api/vehicles
        [HttpPost]
        public void Post(Vehicle vehicle)
        { 
            _vehiclesService.AddVehicle(vehicle);
        }

        // PUT: api/vehicles/bus_1
        [HttpPut("{id}")]
        public ActionResult UpdateVehicle(string id, Vehicle vehicle)
        {
            try
            {
                _vehiclesService.UpdateVehicle(vehicle);
                return Ok();
            }
            catch (VehicleNotFoundException)
            {
                return new StatusCodeResult(500);
            }
        }

        // DELETE: api/vehicles/bus_1
        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle(string id)
        {
            try
            {
                _vehiclesService.RemoveVehicle(id);
               return Ok();
            }
            catch(VehicleNotFoundException)
            {
                return new StatusCodeResult(500);
            }
        }
    }
}
