﻿using Microsoft.AspNetCore.Mvc;
using ParkingAppServer.Interfaces;

namespace ParkingAppServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService _parkingService;

        public ParkingController(IParkingService service)
        {
            _parkingService = service;
        }

        // GET api/parking/balance
        [Route("balance")]
        [HttpGet]
        public ActionResult<double> GetBalance()
        {
            return _parkingService.GetBalance();
        }

        // GET api/parking/lastminuteprofit
        [Route("lastminuteprofit")]
        [HttpGet]
        public ActionResult<double> GetLastMinuteProfit()
        {
            return _parkingService.GetLastMinuteProfit();
        }

        // GET api/parking/freeplaces
        [Route("freeplaces")]
        [HttpGet]
        public ActionResult<int> GetFreePlacesNumber()
        {
            return _parkingService.GetFreePlacesNumber();
        }

        // GET api/parking/occupiedplaces
        [Route("occupiedplaces")]
        [HttpGet]
        public ActionResult<int> GetOccupiedPlacesNumber()
        {
            return _parkingService.GetOccupiedPlacesNumber();
        }
    }
}